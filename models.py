from flask_sqlalchemy import declarative_base
from sqlalchemy import Column, VARCHAR, Boolean, Integer
Base = declarative_base()


class Player(Base):
    __tablename__ = "players"
    id = Column('id', Integer, primary_key=True)
    name = Column("name", VARCHAR)
    active = Column("active", Boolean)
