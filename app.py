from flask import Flask, jsonify, render_template, flash, redirect, request, url_for
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask_sqlalchemy_session import flask_scoped_session
from models import Player
from forms import EliminationForm
import json
import random

engine = create_engine('mysql://root:@localhost/agent')
session_factory = sessionmaker(bind=engine)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost/agent'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'secret'

session = flask_scoped_session(session_factory, app)


@app.route('/')
def show_players():
    players = session.query(Player).filter(Player.active)
    # players = Player.query.all()
    r = [p.name for p in players]
    return jsonify({'players': r})


@app.route('/animation')
def animate():
    messages = json.loads(request.args['messages'])
    players = session.query(Player).filter(Player.active)
    players = [p for p in players]

    info = {}
    for p in random.sample(players, len(players)):
        if str(p.id) in messages['players']:
            info[p.name] = True
        else:
            info[p.name] = False

    for player_id in messages['players']:
        session.query(Player).filter(Player.id == player_id).update({'active': False})
        session.commit()

    return render_template('elimination.html', info=json.dumps(info))


@app.route('/form', methods=['GET', 'POST'])
def show_form():
    players = session.query(Player).filter(Player.active)
    form = EliminationForm(request.form)
    form.players.choices = [(p.id, p.name) for p in players]

    if request.method == 'POST':
        indexes = request.form.getlist("players")
        if indexes:
            message = json.dumps({"players": indexes})
            return redirect(url_for('.animate', messages=message))

    return render_template('players.html', form=form)


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=1234, debug=True)
